<?php
class UserDetailsPage extends Page {

	private static $db = array(
		'EmailTo' => 'Varchar(255)',
		'EmailFrom' => 'Varchar(255)'
	);

	private static $has_one = array(
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->addFieldToTab('Root.EmailSettings',
			EmailField::create('EmailFrom', 'EmailFrom')
		);
		$fields->addFieldToTab('Root.EmailSettings',
			EmailField::create('EmailTo', 'EmailTo')
		);

		return $fields;
	}

}
class UserDetailsPage_Controller extends Page_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
		'UserDetailsForm',
		'addUserDetails'
	);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
		Requirements::javascript(PROJECT_DIR . '/code/javascript/UserDetails.js');
		Requirements::css(SSViewer::get_theme_folder() . '/css/UserDetails.css');

	}

	public function UserDetailsForm() {
		return new UserDetailsForm($this, 'UserDetailsForm');
	}

	public function addUserDetails() {
		if (Director::is_ajax()) {
			$securityID = $this->request->postVar('SecurityID');
			$name = $this->request->postVar('Name');
			$email = $this->request->postVar('Email');
			$phone = $this->request->postVar('Phone');
			$location = $this->request->postVar('Location');
			$beverage = $this->request->postVar('Beverage');
			if ($securityID != Session::get('SecurityID')) {
				return json_encode(array('false 1'));
			}
			// write the data into a new object
			$userDetails = new UserDetails();
			$userDetails->Name = $name;
			$userDetails->Email = $email;
			$userDetails->Phone = $phone;
			$userDetails->Location = $location;
			$userDetails->Beverage = $beverage;
			$userDetails->write();

			$from = 

			// fire off email
			$subject = 'User Application Received';
			$body = "The following application has been received from $name ($email)";
			$email = new Email(
				$this->owner->FromEmail,
				$this->owner->ToEmail,
				$subject,
				$body
			);
			$email->sendPlain();

			return json_encode(array('Beverage' => $beverage));
		}
		return json_encode(array('false 2'));
	}

}
