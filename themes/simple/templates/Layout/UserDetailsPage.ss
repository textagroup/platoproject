<% include SideBar %>
<div class="content-container unit size3of4 lastUnit">
	<article>
		<h1>$Title</h1>
		<div class="content">$Content</div>
	</article>
		$UserDetailsForm
		<p id="formMessage"></p>
		<div id="ajax-loader"></div>
		<div class="beverage" id="Coffee"></div>
		<div class="beverage" id="Tea"></div>
		<div class="beverage" id="CocaCola"></div>
		<div class="beverage" id="Water"></div>
</div>
