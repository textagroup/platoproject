$(document).ready(function () {
	function validateEmail(email) {
		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		return re.test(email);
	}
	$(document).on('click', '#AjaxSubmit',function(e) {
		e.preventDefault();

		// error checking
		var errors = 0;
		var requiredMessage = '<p class="requiredError">This field is required</p>';
		$('.requiredError').remove();
		$('form').find('input').each(function(){
			if($(this).prop('required') && !this.value){
				errors = 1;
				$(this).after(requiredMessage);
			}
			if ($(this).prop('type') == 'email' && this.value) {
				var invalidEmail = '<p class="requiredError">This email address is invalid</p>';
				if (!validateEmail(this.value)) {
					errors = 1;
					$(this).after(invalidEmail);
				}
			}
		});
		if (errors) return;

		// fire off ajax request
		var formSecurityID = $('input[name="SecurityID"]').val();
		var formName = $('input[name="Name"]').val();
		var formEmail = $('input[name="Email"]').val();
		var formPhone = $('input[name="Phone"]').val();
		var formLocation = $('input[name="Location"]').val();
		var formBeverage = $('select[name="Beverage"]').val();
		var data = {
			SecurityID: formSecurityID,
			Name: formName,
			Email: formEmail,
			Phone: formPhone,
			Location: formLocation,
			Beverage: formBeverage
		};
		$('#ajax-loader').show();
		$.ajax({
			url: 'UserDetailsPage_Controller/addUserDetails',
			type: 'POST',
			data: data,
			success: function(result, textStatus, jqXHR) {
				$('#ajax-loader').hide();
				var parsedResult = $.parseJSON(result);
				var beverage = parsedResult.Beverage.replace(/ /g, '');
				$('.beverage').hide();
				$('#' + beverage).show();
				$('#formMessage').text('Your details have been submitted');
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$('#formMessage').text('A error has occured');
				//console.log(errorThrown);
			}
		});
	});
});

