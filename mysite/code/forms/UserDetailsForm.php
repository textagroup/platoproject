<?php

class UserDetailsForm extends Form {
	public function __construct($controller, $name) {
		$fields = new FieldList(
			TextField::create('Name'),
			EmailField::create('Email'),
			TextField::create('Phone'),
			TextField::create('Location'),
			DropdownField::create(
				'Beverage',
				'Beverage',
 				singleton('UserDetails')->dbObject('Beverage')->enumValues()
			)->setEmptyString('Select one')
		);

		$actions = new FieldList(
			FormAction::create('doContact')
			->setAttribute('id' , 'AjaxSubmit')
			->setTitle('Submit')
		);
		$validator = new RequiredFields(array('Name', 'Email', 'Phone', 'Location'));
		parent::__construct($controller, $name, $fields, $actions, $validator);
	}
}
