<?php

global $project;
$project = 'mysite';
define('PROJECT_DIR', $project);

global $database;
$database = 'SS_PlatoProject';

require_once('conf/ConfigureFromEnv.php');

// Set the site locale
i18n::set_locale('en_US');
