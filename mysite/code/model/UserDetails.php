<?php

class UserDetails extends DataObject {
	private static $db = array(
		'Name' => 'Varchar',
		'Email' => 'Varchar(254)',
		'Phone' => 'Varchar',
		'Location' => 'Varchar',
		'Beverage' => "Enum('Coffee, Tea, Coca Cola, Water')"
	);

	private static $summary_fields = array(
		'Name',
		'Email',
		'Phone',
		'Location',
		'Beverage'
	);
}

class UserDetailsAdmin extends ModelAdmin {
	private static $managed_models = array('UserDetails');
	private static $url_segment = 'user-details';
	private static $menu_title = 'User Details';
}
